﻿using System;
using NTaraCollegeFootball.Models;

namespace NTaraCollegeFootball
{
    public class Globals
    {
        public static CollegeFootballTeamList Teams = new CollegeFootballTeamList();
        public const string ALL_COLS = "all";

        public Globals()
        {
        }
    }
}
