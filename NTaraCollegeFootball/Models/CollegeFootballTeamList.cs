﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace NTaraCollegeFootball.Models
{
    public class CollegeFootballTeamList : IEnumerable<CollegeFootballTeamModel>
    {
        //TODO change to env var
        public const string DATAFILE = "App_Data/CollegeFootballTeamWinsWithMascots.csv";

        private List<CollegeFootballTeamModel> teamList = new List<CollegeFootballTeamModel>();
        private List<string> columns = new List<string>();

        public CollegeFootballTeamList()
        {
            try
            {   
                using (StreamReader sr = new StreamReader(DATAFILE))
                {
                    bool readHeader = false;
                    while(sr.Peek() >= 0) {
                        string line = sr.ReadLine();
                        if(readHeader) {
                            this.teamList.Add(new CollegeFootballTeamModel(line, columns));
                        } else {
                            columns.AddRange(line.Split(","));
                            readHeader = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //TODO use proper logger
                Console.WriteLine("CollegeFootballTeamList: Error reading CSV file.");
                Console.WriteLine(e.Message);
            }
        }

        public IEnumerator<CollegeFootballTeamModel> GetEnumerator()
        {
            return this.teamList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.teamList.GetEnumerator();
        }

        public List<string> Columns
        {
            get
            {
                return columns;
            }
        }

        public List<CollegeFootballTeamModel> TeamList
        {
            get
            {
                return teamList;
            }
        }
    }
}
