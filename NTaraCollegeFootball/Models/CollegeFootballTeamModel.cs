﻿using System;
using System.Collections.Generic;

namespace NTaraCollegeFootball.Models
{
    public class CollegeFootballTeamModel
    {
        //header example:
        //Rank,Team,Mascot,Date of Last Win,Winning Percetnage,Wins,Losses,Ties,Games

        private string csvRecord;

        public CollegeFootballTeamModel()
        {

        }

        //This constructor allows the CSV structure to drive the content of the class
        //which has the advantage of flexibility as it will dynamically adapt to additional
        //columns and the like, but disadvantage of being more dependent on the CSV to be
        //well structured and formatted properly (e.g., the mispelling of "percentage" is
        //evident in the UI as a result).  For a small example app like this it seems
        //adequate to illustrate basic ability and pragmatic approach to solving the problem.
        public CollegeFootballTeamModel(string csvRecord, List<string> columns)
        {
            if (!string.IsNullOrWhiteSpace(csvRecord))
            {
                this.csvRecord = csvRecord; //retain for "all" searches
                string[] fields = csvRecord.Split(",");
                if(fields.Length != columns.Count) {
                    //TODO logger
                    return;
                }
                for(int i = 0; i < fields.Length; i++)
                {
                    this[columns[i]] = fields[i];
                }
            }
        }

        private Dictionary<string, string> values = new Dictionary<string, string>();

        public Dictionary<string, string> Values
        {
            get
            {
                return values;
            }
        }
        public string CsvRecord
        {
            get
            {
                return csvRecord;
            }
        }
        //Provid an indexer to allow the CSV header to define our fields
        public string this[string column]
        {
            get
            {
                //by "aliasing" our full csv record in this way, we simplify the query lambda
                //and allow the default select option to be used the same way as column names
                //This is arguably allowing blurring the separation of concerns and allowing 
                //view code to influence the model, but seems an appropriate trade-off in this 
                //case.
                if(column.ToLower() == "all")
                {
                    return csvRecord;
                } else
                {
                    return values[column];
                }
            }
            set
            {
                values[column] = value;
            }
        }
    }
}
