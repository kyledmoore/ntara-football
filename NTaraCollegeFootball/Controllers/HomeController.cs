﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NTaraCollegeFootball.Models;

namespace NTaraCollegeFootball.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            addFormDefaultsToViewBag();
            return View(new List<CollegeFootballTeamModel>());
        }

        public IActionResult Search(string q, string col)
        {
            addFormDefaultsToViewBag(q, col);

            if (!string.IsNullOrWhiteSpace(q))
            {
                //shouldn't be able to get here w/o also supplying the column w/o client
                //manipulation, but that's a possibility so default it.
                col = string.IsNullOrWhiteSpace(col) ? Globals.ALL_COLS : col;
                return View("Index", Globals.Teams.Where(team => team[col].ToLower().Contains(q.ToLower())));
            }

            return View("Index", Globals.Teams);
        }

        private void addFormDefaultsToViewBag()
        {
            addFormDefaultsToViewBag(null, null);
        }

        private void addFormDefaultsToViewBag(string q, string col)
        {
            //These are used only to retain the prior search criteria.  We default them
            //to reduce burden of template code when setting input values.
            ViewBag.q = string.IsNullOrWhiteSpace(q) ? "" : q;
            ViewBag.col = Globals.Teams.Columns.Contains(col) ? col : Globals.ALL_COLS;

            //Should we need more than just this, I would go with an explicit ViewModel
            //instead of using ViewData/ViewBag
            ViewBag.columns = Globals.Teams.Columns;
        }

        public IActionResult About()
        {
            ViewData["Message"] = "About this project.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Kyle Moore";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
