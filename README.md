# ntara-football #

ntara-football is an .NET Core web application completing the Code Exam provided by Ntara

## Approach Used ##

### KISS - Keep It Simple, Stupid.

This assignment was short and sweet.  The solution should be as well.  That meant no unnecessary
fluff, using default routes, and making pragmatic choices on implementation.  

I chose, then, to let the models build their properties dynamically from the CSV file rather than 
a ton of boilerplate getters/setters for the columns.  You probably don't want to sift through that
anyway.  I aimed to show how much I could do with a small amount of code.

### Establish my understanding of OOP

Having worked with a lot of junior developers, a common theme that I have noticed is that individuals
with an otherwise solid educational background in object oriented programming throw good practices
out of the window when they are faced with applying those practices within the confines of a framework.

I wanted to illustrate that I understood the utility of good tier separation and attempted to do that
by keeping the template very light, performing business logic strictly within the model (in this case
that really only amounted to the actual parsing of the CSV file for the most part), and allowing the
controller to perform the filtering of the data.

### Parsing of the CSV

After first Googling to see what stock libraries/classes might be available for doing this in .NET,
I decided providing a solution using a cryptic OLE connection string to present the document as a
database probably wasn't in the spirit of request to "please don't use a database" and didn't do much
to illustrate the core skills I find to be important so didn't see any reason why simply using good
'ol String.split() wouldn't suffice.

I liked the idea of a collection object that was responsible for handling the initial parsing of the
file and construction of the list of records that would be represented by their own dedicated model
for use on the page, so I decided to have CollegeFootballTeamList handle the file reads at the line
level and delegate individual record creation (column level parsing) to the constructor of 
CollegeFootbalTeamModel.

### Search

I was aware of Linq and have use lambdas in the form of anonymous functions in Javascript as well as
the Java8 variant, so wanted to properly illustrate the elegance of the use of that here.  I didn't
want a buch of fiddly logic surrounding filtering by the different columns, so I chose to make use of
C#s indexer feature so that it would be more convenient to index into the object in my lambda
expression.  

I went ahead and exposed the backing dictionary with a public accessor so that it was
more convenient to enumerate the columns within the template without having to hand code those there
as well.  Certainly at the point of introducing individual formatting and styling of those columns
it might in fact be a better choice to place them all in the template direclty instead, but in the 
context of this exercise and in the interest of time it seemed fine to have the nested loop in the
template for the columns.

### Controller Actions and Templates

I didn't see any reason to have a separate template for search and the index page, but I chose to
create a separate Search controller action for the simple reason of simplifying the logic to support
an "open" search, while not loading all record when reaching the index page initially given that 
wasn't implied by the description.  In other words, `HomeController.Index()` provides an empty 
collection to the view, while `HomeController.Search()` will render the same view but supply the
entire list when the form is submitted with empty request parameters.

Alternatively, I might have chosen to have the `Search` action supply a partial template consisting
only of the table of results and submitted the form via AJAX, replacing a search `div` by ID w/ that
returned partial template in a real world app with a larger page where a full refresh was of concern.

### Other Notables and Things I Might Change

In order to facilitate the searching by all columns, I wanted to avoid looping over each field in a
model individually, so I chose to retain the entire record string during the parsing phase so that
a single search against that string could suffice instead.  This was a trade-off of duplicating data
in memory for performance and maintenance and I might not make that choice with a larter data set 
and under tight memory constraints.  I wasn't happy calling that property "all", which would have
been necessary to allow me to pass the default column select box value directly to the lambda, as
that wasn't entirely descriptive when looking at the model, so I compromised and instead added a 
couple lines in the indexer accessor of `CollegeFootballTeamModel` to supply `csvRecord` for the 
"all" index.

I noticed the data header had a typo for "percentage", but chose to ignore it.  In a real world 
situation I would have either corrected the source data or requested the client do so in a situation
where they supplied the CSV on a regular basis via upload or something of that nature.  Barring that
I would add code to correct it, perhaps dynamically through a mapping stored in configuration if it
were a chronic condition with the supplied data.

Additionally, I might choose to calculate the winning percentage vs. display that value directly, and
obviously, some code to format that data as a percentage would be in order vs. the decimal value.